/**
 * contains the app for NFT marketplace
 * @author Tsafack fritz <tsafack07albin@gmail.com>
 */

import { useState, useEffect } from 'react'
import './App.css'
import { pinataGatewayRoot, uploadFileToIPFS, uploadJSONToIPFS } from './Helper/Pinata'
import NFTMarketplaceContract from './Contracts/NFTMarketPlace.json'
import ContractsAddresses from './Helper/ContractsAdresses.json'
import { ethers } from 'ethers';
import Navbar from './Components/Navbar/Navbar'
import NftCard from './Components/NftCard/NftCard'
import profil from './assets/profil.png'
import axios from 'axios'
import Popup from './Components/Popup/Popup'
import Switch from './Components/Switch/Switch'


function App() {

  const [file, setfile] = useState(null)
  const [price, setprice] = useState(null)
  const [title, settitle] = useState(null)
  const [name, setname] = useState(null)
  const [description, setdescription] = useState(null)
  const [collection, setcollection] = useState(null)
  const [nfts, setnfts] = useState(null)
  const [address, setaddress] = useState(null)
  const [open, setopen] = useState(null)
  const [currentNft, setcurrentNft] = useState(null)
  const [loading, setloading] = useState(null)
  const [isUpdate, setisUpdate] = useState(false)
  const [checked, setchecked] = useState(null)

  /**
   * get the connected wallet address
   */
  const getAddress = async()=>{
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const address = await signer.getAddress();
    setaddress(address)
  }

  const handleFileChange = (e)=>{
    setfile(e.target.files[0])
  }

  /**
   * get the list of the connected account nfts
   */
  const listMyNFTs = async ()=>{
    setloading(true)
    settitle("My NFTs")
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    const signer = provider.getSigner()
    const nFTMarketplaceContract = new ethers.Contract(
      ContractsAddresses.NFTMarketplace, 
      NFTMarketplaceContract.abi, signer
    )
    let listingPrice = await nFTMarketplaceContract.getListPrice();
    console.log(listingPrice.toString());
    nFTMarketplaceContract.connect(signer)
    let transaction = await nFTMarketplaceContract.getMyNFTs()
    await displayNFTs(transaction, nFTMarketplaceContract)
    setloading(false)
  }

  /**
   * get the list of all nfts on sale
   */
  const listAllNFTs = async ()=>{
    setloading(true)
    settitle("Marketplace NFTs")
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    const signer = provider.getSigner()
    const nFTMarketplaceContract = new ethers.Contract(
      ContractsAddresses.NFTMarketplace, 
      NFTMarketplaceContract.abi, signer
    )
    nFTMarketplaceContract.connect(signer)
    let transaction = await nFTMarketplaceContract.getAllNFTs()
    console.log(transaction)
    let diplayNFT = transaction.filter((item)=> item.currentlyListed === true)
    await displayNFTs(diplayNFT, nFTMarketplaceContract)
    setloading(false)
  }

  /**
   * get nft token uri an update state
   * 
   * @param transaction 
   * @param nFTMarketplaceContract 
   */
  const displayNFTs = async(transaction, nFTMarketplaceContract)=>{
    let items = []
    for (const item of transaction) {
      let tokenURI = await nFTMarketplaceContract.tokenURI(item.tokenId);
      console.log(tokenURI);
      let meta = await axios.get(tokenURI);
      let metadata;
      try {
        metadata = JSON.parse(meta.data.data)
      } catch (error) {
        // error while parsing
        // console.log(error);
        metadata = null
      }
      console.log(metadata);
      if(metadata){
        let price = ethers.utils.formatUnits(item.price.toString(), 'ether');
        let token = {
          price,
          tokenId: item.tokenId.toNumber(),
          seller: item.seller,
          owner: item.owner,
          image: metadata.image,
          name: metadata.name,
          collection: metadata.collection,
          description: metadata.description,
          currentlyListed: item.currentlyListed
        }
        items.push(token)
      }
    }
    console.log(items);
    setnfts(items)
  }
  /**
   * create an NFT
   * 
   */
  const upload = async()=>{
    // check if all field are filled
    if(!name ||!price ||!description ||!collection ||!file){
      alert("all fields are required");
      return;
    }

    // upload file
    let ipfsHash = await uploadFileToIPFS(file);
    let imageUrl = `${pinataGatewayRoot}${ipfsHash}`;

    // convert price to wei
    let weiPrice = ethers.utils.parseUnits(price, 'ether');

    // meta data to store on IPFS
    const metadata = {
      name,
      collection,
      description,
      image: imageUrl,
      price: price.toString()
    }

    // upload json to ipfs
    let jsonUrl = await uploadJSONToIPFS(metadata);
    jsonUrl = `${pinataGatewayRoot}${jsonUrl}`;

    // create NFT on blockchain
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    const signer = provider.getSigner()
    const nFTMarketplaceContract = new ethers.Contract(
      ContractsAddresses.NFTMarketplace, 
      NFTMarketplaceContract.abi, 
      signer
    )
    let listingPrice = await nFTMarketplaceContract.getListPrice();
    console.log(listingPrice.toString());
    nFTMarketplaceContract.connect(signer)
    let tx = await nFTMarketplaceContract.createToken(jsonUrl, weiPrice, {value: listingPrice.toString()})
    // close popup
    setopen(false)
    // list user nfts
    await listMyNFTs()
  }

  const executeSale = async(tokenId, price)=>{
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    const signer = provider.getSigner()
    const nFTMarketplaceContract = new ethers.Contract(
      ContractsAddresses.NFTMarketplace, 
      NFTMarketplaceContract.abi, signer
    )
    nFTMarketplaceContract.connect(signer)
    let transaction = await nFTMarketplaceContract.executeSale(tokenId, {value: ethers.utils.parseUnits(price, 'ether')})
    await listMyNFTs()
  }

  const updateTokenInfos = async(tokenId)=>{
    if(checked !== null){
      let weiPrice = ethers.utils.parseUnits(currentNft.price, 'ether');
      const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
      const signer = provider.getSigner()
      const nFTMarketplaceContract = new ethers.Contract(
        ContractsAddresses.NFTMarketplace, 
        NFTMarketplaceContract.abi, signer
      )
      nFTMarketplaceContract.connect(signer)
      let transaction = await nFTMarketplaceContract.updateTokenInfos(tokenId, checked, weiPrice)
      await listMyNFTs()        
    }
  }

  /**
   * connect the app an evently switch the chain
   */
  const connectApp = async()=>{
    // get chain id
    const chainId = await window.ethereum.request({method: 'eth_chainId'})
    if(chainId == '0x5'){
      // switch network to goerli
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{chainId: '0x5'}],
      })
    }
    // update current address when account change
    await window.ethereum.request({method: 'eth_requestAccounts'})
    .then(()=>{
      // update button
      getAddress()
    })
  }

  useEffect(()=>{
    let connected = window.ethereum.isConnected();
    if(connected){
      getAddress()
      // listAllNFTs()
      // toggleConnectButton(connected)
    }
    window.ethereum.on('accountsChanged', function(accounts){
      window.location.replace(location.pathname)
    })
  }, []);

  return(
    <div className='wrapper'>
      <Navbar 
      connectApp={connectApp} 
      address={address}
      listAllNFTs={listAllNFTs}
      listMyNFTs={listMyNFTs}
      setopen={setopen}
      setnfts={setnfts} />
      {loading && <center><h1>Loading Please wait . . .</h1></center>}
      <div className="container">
        <h2>{title ? title : ""}</h2>
        <div className="nft_list">
          {nfts && nfts.map((nft, i) =><NftCard setcurrentNft={setcurrentNft} key={i} nft={nft} />)}
        </div>
          {!nfts && 
          <div className='about'>
            <img className='about_image' src={profil} />
            <div>
              <h2>About DD NFT Marketplace</h2>
              <p>It's an NFT market place running on the goerli testnet. I've developed it just for fun :)</p><br /><br />
              <h4>About me</h4>
              <p>Name: Tsafack Djiogo Fritz Albin</p>
              <p>Portfolio: <a href="https://tsafack-fritz.netlify.app">https://tsafack-fritz.netlify.app</a></p>
              <p>Gitlab: <a href="https://gitlab.com/dump501">https://gitlab.com/dump501</a></p>
              <p>Email: tsafack07albin@gmail</p>
              <p>Location: Dschang, Cameroon</p>
            </div>
          </div>}
      </div>

      {/* popups  */}
      {open && 
      <Popup closePopup={()=>setopen(false)}>
        {loading && <center><h1>Loading Please wait . . .</h1></center>}
        <div className='nft_form'>
          <div className="form_upload">
            <input onChange={handleFileChange} type="file" name="" id="" />
            <span>upload NFT image</span>
          </div>
          <div className="form_fields">
            <input onChange={(e)=>{setname(e.target.value)}} 
              type="text" className="form_field" placeholder="Name" />
            <input onChange={(e)=>{setcollection(e.target.value)}} 
              type="text" className="form_field" placeholder="Collection" />
            <input onChange={(e)=>{setdescription(e.target.value)}} 
              type="text" className="form_field" placeholder="Description" />
            <input onChange={(e)=>{setprice(e.target.value)}} 
              type="number" className="form_field" placeholder="price in eth" />
            <button onClick={(e)=>{upload()}}>Create NFT</button>&nbsp;
            <button onClick={(e)=>{setopen(false)}}>Cancel and close</button>
          </div>
        </div>
      </Popup>}
      {currentNft && 
      <Popup>
        <div className='nft_detail'>
        {loading && <center><h1>Loading Please wait . . .</h1></center>}
          <img className='nft_detail_image' src={currentNft.image} />
          <div>
            <p><h4>Name</h4>{currentNft.name}</p>
            <p><h4>Collection</h4>{currentNft.collection}</p>
            <p><h4>Description</h4>{currentNft.description}</p>
            <p><h4>Price</h4>{currentNft.price} eth</p><br />
            <div>
              {address === currentNft.seller ? 
              <button onClick={(e)=>{setisUpdate(true)}}>update</button> : 
              <button onClick={(e)=>{executeSale()}}>Buy</button>} &nbsp;
              <button onClick={(e)=>{setcurrentNft(null)}}>X close</button>
            </div>
          </div>
          {address === currentNft.seller && isUpdate && <div>
            <h3>Update nft</h3><br />
            selling status
            <Switch status={currentNft.currentlyListed} setchecked={setchecked} />
            <button onClick={(e)=>{updateTokenInfos(currentNft.tokenId)}}>save</button>
          </div>}
        </div>
      </Popup>}
    </div>
  )
}

export default App
