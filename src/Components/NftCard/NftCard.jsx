import React from 'react'
import "./NftCard.css"

function NftCard({nft, setcurrentNft}) {
  return (
    <div className='nftCard'>
        <img src={nft.image} />
        <div className='nftCard_text'>
            <div className='nft_name'>{nft.name}</div>
            <div className='nft_collection'>{nft.collection}</div>
            <div className='nft_actions'>
                <button onClick={e=>setcurrentNft(nft)}>View</button>
                <div>{nft.price} eth</div>
            </div>
        </div>
    </div>
  )
}

export default NftCard