import React from 'react'
import { useState } from 'react'
import "./Switch.css"

function Switch({status, setchecked}) {
  const [isChecked, setisChecked] = useState(status?status:false)
  return (
    <input onChange={(e)=>{setisChecked(!isChecked); setchecked(!isChecked)}} type="checkbox" className='switch' checked={isChecked} />
  )
}

export default Switch