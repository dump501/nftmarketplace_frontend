import React from 'react'
import './Navbar.css'

function Navbar({connectApp, address, listAllNFTs, listMyNFTs, setopen, setnfts}) {
  return (
    <div className='navbar'>
        <div className='navbar_left'>
            <p className='navbar_title'>DD NFT Marketplace</p>
            {address && <p>&nbsp; {address}</p>}
        </div>
        <div className='navbar_search'>
            {/* <input className='navbar_search_input' type="text" placeholder='search NFT or collection' /> */}
        </div>
        <div className='navbar_right'>
            <a onClick={(e)=>{e.preventDefault(); setnfts(null)}} className='navbar_link' href="#">
                <div className='navbar_item'>
                    Home
                </div>
            </a>
            <a onClick={(e)=>{e.preventDefault(); setopen(true)}} className='navbar_link' href="#">
                <div className='navbar_item'>
                    Create NFT
                </div>
            </a>
            <a onClick={(e)=>{e.preventDefault(); listAllNFTs()}} className='navbar_link' href="#">
                <div className='navbar_item'>
                    NFT market
                </div>
            </a>
            <a onClick={(e)=>{e.preventDefault(); listMyNFTs()}} className='navbar_link' href="#">
                <div className='navbar_item'>
                    My NFTs
                </div>
            </a>
            {address ? "Wallet connected" :<button onClick={e=>connectApp()}>Connect wallet</button>}
        </div>
    </div>
  )
}

export default Navbar