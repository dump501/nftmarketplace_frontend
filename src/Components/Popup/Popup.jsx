import React from 'react'
import "./Popup.css"

function Popup({children, closePopup}) {
  return (
    <div className="popup-container">
     <div className="popup-body">
        {children}
     </div>
    </div>
  )
}

export default Popup