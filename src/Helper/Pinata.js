/**
 * contains functions to upload file or json to pinata
 * @author Tsafack fritz <tsafack07albin@gmail.com>
 */

import * as env from "../env.json"
import axios from 'axios'
const { pinata_key, pinata_secret } = env;

/**
 * upload a file to the ipfs pinata :)
 * @param {*} file 
 * @returns the ipfs hash of the file uploaded
 */
export const uploadFileToIPFS = async(file)=>{
    const url = "https://api.pinata.cloud/pinning/pinFileToIPFS";
    try {
        let data = new FormData()
        data.append("file", file)
        var config = {
            method: 'post',
            url: 'https://api.pinata.cloud/pinning/pinFileToIPFS',
            headers: { 
                pinata_api_key: pinata_key,
                pinata_secret_api_key: pinata_secret,
            },
            data : data
          };
          
          const response = await axios(config);
          console.log(response);
          if(response.status == 200){
            return response.data.IpfsHash;
          }
          alert("problem while uploading");
    } catch (error) {
        
    }
}

/**
 * upload 
 * @param {*} metadata 
 * @returns the ipfs hash of the json uploaded
 */
export const uploadJSONToIPFS = async(metadata)=>{
    const url = "https://api.pinata.cloud/pinning/pinJSONToIPFS";

    try {
        let data = JSON.stringify(metadata)
        var config = {
            method: 'post',
            url: 'https://api.pinata.cloud/pinning/pinJSONToIPFS',
            headers: { 
                pinata_api_key: pinata_key,
                pinata_secret_api_key: pinata_secret,
            },
            data : {data}
          };
          
          const response = await axios(config);
          
          console.log(response.data);
          if(response.status == 200){
            return response.data.IpfsHash;
          }
        
    } catch (error) {
        
    }
}

/**
 * the pinata gateway root url
 */
export const pinataGatewayRoot = "https://gateway.pinata.cloud/ipfs/";